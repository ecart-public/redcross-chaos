<?php
namespace Chaos\Auth;

use Chaos\Models\CSDevice;
use Chaos\Models\CSRoleMenu;
use Chaos\Models\CSRoleUser;
use Chaos\Models\CSUser;
use Chaos\Auth\TokenService;
use Chaos\Auth\RefreshService;
use Chaos\Notifications\DeviceChangeNotification;
use Chaos\Notifications\OtpNotification;
use Chaos\Support\Response;
use Illuminate\Support\Facades\Notification;

class Authenticator
{
    use Providers\EmailAndPassword,
        Providers\AuthorizationCode;
    
    /**
     * @var TokenService
     */
    protected $accessService;
    
    /**
     * @var RefreshService
     */
    protected $refreshService;
    
    /**
     * @var OtpService
     */
    protected $otpService;
    
    /**
     * @var array
     */
    protected $data = [];
    
    public function __construct(array $data = [])
    {
        $this->data = $data;
        $this->accessService = new TokenService();
        $this->refreshService = new RefreshService();
        $this->otpService = new OtpService();
    }
    
    public function getRefreshToken(CSUser $user)
    {
        $this->uid = $user->guid;
        return ($this->refreshService)
            ->setUid($user->guid)
            ->addValue("uid", $user->guid)
            ->addValue("name", $user->name)
            ->make();
    }
    
    public function getAccessToken(CSUser $user)
    {
        return ($this->accessService)
            ->addValue("uid", $user->guid)
            ->addValue("name", $user->name)
            ->make();
    }
    
    public function getIDToken(CSUser $user)
    {
        return ($this->accessService)
            ->addValue("uid", $user->guid)
            ->addValue("name", $user->name)
            ->addValue("email", $user->email)
            ->addValue("user", $this->getUser($user))
            ->addValue("role", $this->getRole($user))
            ->addValue("roles", $this->getRoles($user))
            ->addValue("menus", $this->getMenu($user))
            ->addValue("permissions", [])
            ->make();
    }
    
    public function getUser(CSUser $user)
    {
        return [
            "uid" => $user->guid,
            "name" => $user->name,
            "picture" => $user->picture["url"],
            "email" => $user->email
        ];
    }
    
    public function getMenu(CSUser $user)
    {
        return CSRoleMenu::all();
    }
    
    public function getRole(CSUser $user)
    {
        return $this->getRoles($user)->first();
    }
    
    public function getRoles(CSUser $user)
    {
        return CSRoleUser::getByUid($user->guid)
            ->map(function($role) {
                return [
                    "guid" => $role->guid,
                    "code" => $role->role,
                    "name" => $role->name,
                ];
            });
    }
    
    public function signOut()
    {
        return response()->json(["message" => "Signed out"]);
    }
    
    public function switchTo()
    {
        $data = (object) $this->data;
        $uid = TokenService::getClaim("uid");
        
        // Clear Primary
        CSRoleUser::where("uid", $uid)
            ->update(["is_primary" => false]);
        
        // Set Primary
        CSRoleUser::where("uid", $uid)
            ->where("guid", $data->guid)
            ->update(["is_primary" => true]);
        
        $user = CSUser::findByGuid($uid);
        return $this->render($user);
    }
    
    public function refreshToken()
    {
        $refresh = $this->refreshService;
        if (!$refresh->validate()) {
            return Response::Unauthorized([], "หมดอายุการใช้งาน");
        }
        
        $session = (object) $refresh->getSession();
        $user = CSUser::findByGuid($session->uid);
        if (!$user) {
            return Response::Unauthorized([], "ไม่พบข้อมูลผู้ใช้งาน");
        }
        
        return $this->output([
            "access_token" => $this->getAccessToken($user),
            "refresh_token" => $this->getRefreshToken($user),
            "id_token" => $this->getIDToken($user),
        ]);
    }
    
    public function requestOtp(CSUser $user) 
    {
        $data = $this->otpService->makeOtp([
            "uid" => $user->guid
        ]);
        
        $referCode = $data["key"];
        $otp = $data["otp"];
        
        Notification::sendNow($user, new OtpNotification($referCode, $otp));
        return $referCode;
    }
    
    public function verifyOtp() 
    {
        $data = (object) $this->data;
        $storedOtp = (object) $this->otpService->getOtp($data->code);
        $result = $this->otpService->verifyOtp($data->code, $data->otp);
        
        if (!$result) {
            return Response::Unauthorized([], "Invalid OTP");
        }
     
        $user = CSUser::findByGuid($storedOtp->data['uid']);
        $this->markTrustDevice($user);
        return $this->render($user);
    }
    
    public function markTrustDevice(CSUser $user): bool
    {
        $result = CSDevice::updateOrCreate([
            "uid" => $user->guid,
            "device" => TokenService::getDevice()
        ], [
            "status" => true
        ]);
        
        Notification::send($user, new DeviceChangeNotification());
        return (bool) $result;
    }
    
    public function isTrustDevice(CSUser $user): bool
    {
        if(!env("OTP_REQUIRED", false)) {
            return true;
        }
        
        $device = CSDevice::where("uid", $user->guid)
            ->where("device", TokenService::getDevice())
            ->first();
            
        return $device
            ? $device->status
            : false;
    }
    
    public function render(CSUser $user)
    {
        if (!$this->isTrustDevice($user)) {
            $referCode = $this->requestOtp($user);
            $data = [
                "code" => "OTP_REQUIRED",
                "uid" => $user->guid,
                "name" => $user->name,
                "email" => $user->email,
                "ip" => request()->ip(),
                "device" => TokenService::getDevice(),
                "refer_code" => $referCode,
                "short_code" => strtoupper(substr($referCode, 0, 4))
            ];
            return response()->json($data, 400);
        }
        
        return $this->output([
            "access_token" => $this->getAccessToken($user),
            "refresh_token" => $this->getRefreshToken($user),
            "id_token" => $this->getIDToken($user),
        ]);
    }
    
    public function output(array $data)
    {
        return response()->json($data);
    }
    
}