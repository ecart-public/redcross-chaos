<?php
namespace Chaos\Auth;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;


class AuthorizeService
{
    public function generateKey(): string
    {
        return Str::random(32);
    }
    
    public function setKey(string $key, array $data, int $ttl = 300): void
    {
        Cache::put("{$key}", $data, $ttl);
    }
    
    public function getKey(string $key): array|null
    {
        return Cache::get("{$key}");
    }
    
    public function deleteKey(string $key): void
    {
        Cache::forget("{$key}");
    }
    
    public function verify(string $key): bool
    {
        $data = $this->getKey($key);
        
        if (!$data) {
            return false;
        }
        
        return true;
    }
    
}