<?php
namespace Chaos\Auth;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;


class OtpService
{
    /**
     * @var string
     */
    protected $prefix = "cs_otp";
    
    public function generateOtp(): string
    {
        return rand(100000, 999999);
    }
    
    public function generateOtpKey(): string
    {
        return Str::random(32);
    }
    
    public function setOtp(string $key, string $otp, array $data, int $ttl = 300): void
    {
        $data = [
            "otp" => $otp,
            "ip" => request()->ip(),
            "user_agent" => request()->userAgent(),
            "data" => $data
        ];
        Cache::put("{$this->prefix}:{$key}", $data, $ttl);
    }
    
    public function getOtp(string $key): array|null
    {
        return Cache::get("{$this->prefix}:{$key}");
    }
    
    public function deleteOtp(string $key): void
    {
        Cache::forget("{$this->prefix}:{$key}");
    }
    
    public function verifyOtp(string $key, string $otp): bool
    {
        $data = $this->getOtp($key);
        
        if (!$data) {
            return false;
        }
        
        if ($data["otp"] !== $otp) {
            return false;
        }
        
        if ($data["ip"] !== request()->ip()) {
            return false;
        }
        
        if ($data["user_agent"] !== request()->userAgent()) {
            return false;
        }
        
        $this->deleteOtp($key);
        return true;
    }
    
    public function makeOtp(array $data, int $ttl = 300): array
    {
        $key = $this->generateOtpKey();
        $otp = $this->generateOtp();
        
        $this->setOtp($key, $otp, $data, $ttl);
        
        return [
            "key" => $key,
            "otp" => $otp
        ];
    }
    
}