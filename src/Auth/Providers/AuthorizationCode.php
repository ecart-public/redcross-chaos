<?php

namespace Chaos\Auth\Providers;

use Chaos\Auth\AuthorizeService;
use Chaos\Models\CSUser;
use Chaos\Support\Response;

trait AuthorizationCode {
    
    
    public function signInAuthorizationCode()
    {
        $data = (object) $this->data;
        $code = $data->authorization_code;
        
        $authorizeService = new AuthorizeService();
        $storedKey = (object) $authorizeService->getKey($code);
        $result = $authorizeService->verify($code);
        
        if (!$result) {
            return Response::Unauthorized([], "Authorization code not found");
        }
        
        $user = CSUser::findByGuid($storedKey->uid);
        return $this->render($user);
    }
    
}