<?php

namespace Chaos\Auth\Providers;

use Chaos\Models\CSUser;
use Chaos\Notifications\LoginNotification;
use Hash;
use Chaos\Support\Response;
use Chaos\Models\CSAuth;
use Illuminate\Support\Facades\Notification;

trait EmailAndPassword {
    
    public function signInEmailAndPassword()
    {
        $data = (object) $this->data;
        
        if (!isset($data->username) || empty($data->username) || strlen($data->username) < 4) {
            return Response::BadRequest([], "กรุณากรอกชื่อผู้ใช้งาน");
        }
        
        if (!isset($data->password) || empty($data->password) || strlen($data->password) < 6) {
            return Response::BadRequest([], "กรุณากรอกรหัสผ่าน (ไม่น้อยกว่า 6 ตัวอักษร)");
        }
        
        $auth = CSAuth::findActiveByUsername($data->username);
        
        if (!isset($auth)) {
            return Response::Unauthorized(["username" => $data->username], "ไม่พบข้อมูลผู้ใช้งาน");
        }
        
        if (!CSAuth::attempt($data->username)) {
            return Response::Unauthorized([
                "username" => $data->username,
                "attempt" => $auth->attempt,
                "attempt_at" => $auth->attempt_at
            ], "บัญชีผู้ใช้งานถูกระงับชั่วคราว");
        }
        
        $auth->resetAttempt($data->username);
        
        if ($auth->expire_at && $auth->expire_at < time()) {
            return Response::Unauthorized(["username" => $data->username], "บัญชีผู้ใช้งานหมดอายุ");
        }
        
        if (!Hash::check($data->password, $auth->password)) {
            return Response::Unauthorized(["username" => $data->username], "รหัสผ่านไม่ถูกต้อง");
        }
        
        $user = CSUser::findByGuid($auth->uid);
        
        Notification::send($user, new LoginNotification());
        return $this->render($user);
    }
    
}