<?php
namespace Chaos\Auth;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

use Chaos\Auth\TokenService;

class RefreshService
{
    /**
     * @var string
     */
    protected $prefix = "cs_session";
    
    /**
     * @var string
     */
    protected $token;
    
    /**
     * @var string
     */
    protected $clientToken;
    
    /**
     * @var string
     */
    protected $uid;
    
    /**
     * @var array
     */
    protected $data = [];
    
    /**
     * @var TokenService
     */
    protected $accessService;
    
    public function __construct()
    {
        $this->accessService = new TokenService();
    }
    
    public function getClientToken(): string|null {
        return $this->clientToken ?? Request::header("X-Refresh-Token");
    }
    
    public function setClientToken(string $token): self
    {
        $this->clientToken = $token;
        return $this;
    }
    
    public function setUid(string $uid): self
    {
        $this->uid = $uid;
        return $this;
    }
    
    public function getUid(): string|null {
        return $this->uid;
    }
    
    public function addValue(string $key, $value): self
    {
        $this->data[$key] = $value;
        return $this;
    }
    
    public function getValue(string $key, $default = null)
    {
        return $this->data[$key] ?? $default;
    }
    
    public function generateToken(): string {
        $result = Str::random(32);
        $device = $this->accessService->getDevice();
        
        return base64_encode(hash("sha256", "{$result}::{$device}"));
    }

    public function getRenewDays(): int {
        return config("chaos.auth.renew_days", 3);
    }
    
    public function getExpireDays(): int {
        return config("chaos.auth.refresh_days", 7);
    }
    
    public function isShoudRefresh(): bool {
        if(!$this->getClientToken()) return true;
        
        $session = (object) $this->getSession();
        return $session === null 
            || $session->device !== $this->accessService->getDevice()
            || $session->ip !== $this->accessService->getIp()
            || $session->renew_at < time();
    }
    
    public function make(): string
    {
        if ($this->isShoudRefresh()) {
            $this->token = $this->generateToken();
            $this->createSession($this->token, [
                "uid" => $this->uid,
                "token" => $this->token,
                "data" => $this->data,
                "device" => $this->accessService->getDevice(),
                "ip" => request()->ip(),
                "user_agent" => request()->userAgent(),
                "renew_at" => now()->addDays($this->getRenewDays())->getTimestamp(),
                "expire_at" => now()->addDays($this->getExpireDays())->getTimestamp()
            ]);
            return $this->token;
        } else {
            $session = (object) $this->getSession();  
            $this->token = $session->token;
            return $this->token;
        }
    }
    
    public function createSession(string $token, array $data): bool {
        return Cache::put("{$this->prefix}::{$token}", $data, now()->addDays(7));
    }
    
    public function updateSession(string $token, array $data, string $oldToken = null): bool {
        if ($oldToken) {
            $this->clearCache($oldToken);
        }
        return Cache::put("{$this->prefix}::{$token}", $data, now()->addDays(7));
    }
    
    public function getSession(string $token = null): array|null {
        $token = $token ?? $this->getClientToken();
        if (!$token) {
            return null;
        }
        
        $result = Cache::get("{$this->prefix}::{$token}");
        return (array) $result;
    }
    
    public function clearCache(string $token = null): bool {
        $token = $token ?? $this->getClientToken();
        return Cache::forget("{$this->prefix}::{$token}");
    }

    public static function validate(): bool {
        return (new self)->getSession() !== null;
    }

}
