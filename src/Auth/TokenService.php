<?php

namespace Chaos\Auth;

use DateInterval;
use DateTimeImmutable;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;

use Lcobucci\Clock\SystemClock;
use Lcobucci\JWT\Signer\Ecdsa\Sha512;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Token\DataSet;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\UnencryptedToken;
use Lcobucci\JWT\Validation\Constraint\IssuedBy;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Validation\Constraint\StrictValidAt;
use Lcobucci\JWT\Validation\Validator;

class TokenService
{
    /**
     * @var array
     */
    protected $errors = [];
    
    /**
     * @var string
     */
    protected $clientId = "default";
    
    /**
     * @var string
     */
    protected $clientToken = null;
    
    /**
     * @var string
     */
    protected $clientKey = null;
    
    /**
     * @var array
     */
    protected $scope = [];
    
    /**
     * @var string
     */
    protected $state = "default";
    
    /**
     * @var array
     */
    protected $data = [];
    
    /**
     * Generate a new private and public key pair
     */
    public static function generateKeys()
    {
        $res = openssl_pkey_new([
            "digest_alg" => "sha512",
            "private_key_bits" => 4096,
            "private_key_type" => OPENSSL_KEYTYPE_EC
        ]);
        
        // Extract the private key from $res to $privKey
        openssl_pkey_export($res, $privKey);
        
        // Extract the public key from $res to $pubKey
        $pubKey = openssl_pkey_get_details($res);
        $pubKey = $pubKey["key"];
        
        return [
            "private" => $privKey,
            "public" => $pubKey
        ];
    }
    
    /**
     * Encrypt the data to $encrypted using the public key
     */
    public static function encrypt(string $data, string $pubKey): string
    {
        openssl_public_encrypt($data, $encrypted, $pubKey, OPENSSL_PKCS1_OAEP_PADDING);
        return $encrypted;
    }
    
    /**
     * Decrypt the data using the private key and store the results in $decrypted
     */
    public static function decrypt(string $encrypted, string $privKey): string
    {
        openssl_private_decrypt($encrypted, $decrypted, $privKey);
        return $decrypted;
    }

    /**
     * Add an error message
     */
    public function addError(string $message, array $contexts = []): self
    {
        $this->errors[] = $message;
        Log::error($message, $contexts);
        return $this;
    }
    
    /**
     * Get all error messages
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Get the signer
     */
    public function getSigner(): Sha512
    {
        return new Sha512();
    }
    
    /**
     * Get the private key
     */
    public function getPrivateKey(): Key
    {
        $path = config("chaos.auth.ssl.private");
        return InMemory::file($path);
    }

    /**
     * Get the public key
     */
    public function getPublicKey(): Key
    {
        $path = config("chaos.auth.ssl.public");
        return InMemory::file($path);
    }

    /**
     * Get the configuration
     */
    public function getConfiguration(): Configuration
    {
        return Configuration::forAsymmetricSigner(
            $this->getSigner(),
            $this->getPrivateKey(),
            $this->getPublicKey()
        );
    }

    /**
     * Get the signed with
     */
    public function getSignedWith(): SignedWith {
        $config = $this->getConfiguration();
        return new SignedWith($config->signer(), $config->verificationKey());
    }

    /**
     * Get the can only be used after
     */
    public function getCanOnlyBeUsedAfter(): DateInterval
    {
        $nbf = config("chaos.auth.nbf", 120);
        return new DateInterval("PT{$nbf}S");
    }

    /**
     * Get the expire at
     */
    public function getExpireAt(): DateInterval
    {
        $expireIn = config("chaos.auth.expire_in", 1200);
        return new DateInterval("PT{$expireIn}S");
    }
    
    public function setClientId(string $clientId): self
    {
        $this->clientId = $clientId;
        return $this;
    }
    
    public function getClientId(): string
    {
        return $this->clientId;
    }
    
    public function getClientToken(): Token|UnencryptedToken|null
    {
        $token = $this->clientToken ?? Request::bearerToken();
        if(!$token) return null;

        $config = $this->getConfiguration();
        return $config->parser()->parse($token);
    }
    
    public function setClientKey(string $key): self
    {
        $this->clientKey = $key;
        return $this;
    }
    
    public function getClientKey(): string
    {
        return $this->clientKey;
    }
    
    public function setClientToken(string $token): self
    {
        $this->clientToken = $token;
        return $this;
    }
    
    public function addValue(string $key, $value): self
    {
        $this->data[$key] = $value;
        return $this;
    }
    
    public function getValue(string $key, $default = null)
    {
        return $this->data[$key] ?? $default;
    }
    
    public function setScope(array $scope): self
    {
        $this->scope = $scope;
        return $this;
    }
    
    public function getScope(): array
    {
        return $this->scope;
    }
    
    public function setState(string $state): self
    {
        $this->state = $state;
        return $this;
    }
    
    public function getState(): string
    {
        return $this->state;
    }

    public function make(): string
    {
        $now = new DateTimeImmutable();
        $config = $this->getConfiguration();
        $host = config("chaos.auth.host", "https://ngchaos.app");
        $builder = $config->builder()
            ->issuedBy($host)
            ->permittedFor($this->clientId)
            ->issuedAt($now)
            ->identifiedBy($this->getDevice())
            ->canOnlyBeUsedAfter($now->sub($this->getCanOnlyBeUsedAfter()))
            ->expiresAt($now->add($this->getExpireAt()));

        foreach ($this->data as $key => $value) {
            $builder->withClaim($key, $value);
        }

        $this->clientToken = $builder->getToken($config->signer(), $config->signingKey())->toString();
        return $this->clientToken;
    }
    
    /**
     * Get the token
     */
    public function expireIn(): int
    {
        $token = $this->getClientToken();
        if($token && $token instanceof UnencryptedToken) {
            $now = new DateTimeImmutable();
            $exp = $token->claims()->get("exp");
            return $exp->getTimestamp() - $now->getTimestamp();
        }
        return 0;
    }
    
    
    public static function getDevice(): string|null {
        $devices = [
            Request::header("X-Client-Key"),
            Request::userAgent()
        ];
        return hash("sha256", implode(":", $devices));
    }
    
    public static function getIp(): string {
        return Request::ip();
    }
    
    public function getStrictValidAt(): StrictValidAt
    {
        $clock = new SystemClock(new \DateTimeZone('UTC'));
        return new StrictValidAt($clock);
    }
    
    public function getIssueBy(): IssuedBy
    {
        $host = config("chaos.auth.host", "https://pgw.ecart2u.app");
        return new IssuedBy($host);
    }

    public static function validate(array $roles = []): bool
    {
        $jwt = new self;
        $token = $jwt->getClientToken();
        
        // Check token
        if (!$token) {
            return false;
        }
        
        $result = (new Validator())->validate(
            $token, 
            $jwt->getSignedWith(),
            $jwt->getStrictValidAt(),
            $jwt->getIssueBy()
        );
        
        // Check roles
        if ($result && count($roles)) {
            $role = $token->claims()->get("role");
            return in_array($role, $roles);
        }
        
        return $result;
    }

    public static function getClaims(): DataSet|null
    {
        $token = (new self)->getClientToken();
        return $token ? $token->claims() : null;
    }

    public static function getClaim(string $name, $default = null)
    {
        $claims = self::getClaims();
        return $claims ? $claims->get($name, $default) : null;
    }

}
