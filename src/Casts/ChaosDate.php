<?php
 
namespace Chaos\Casts;
 
use Carbon\Carbon;
use DateTimeInterface;
 
class ChaosDate
{
    public static function convert(?string $date): string|null {
        return $date 
            ? Carbon::parse($date)->format(DateTimeInterface::ISO8601)
            : null;
    }
    
}