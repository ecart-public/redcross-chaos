<?php

namespace Chaos\Commands;

use Illuminate\Console\Command;
use Chaos\Models\CSMenu as BaseModel;

class ChaosMenu extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chaos:menu {--delete}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $prefix = "chaos";
    
    protected $data = [];

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->prefix = "main";
        $this->data = config("chaos.menu") ?? [];
        
        // Delete all menu
        if ($this->option("delete")) {
            $this->info("Deleting all menus...");
            BaseModel::truncate();
            
            $this->info("[OK] Delete menu successfully!");
            return 0;
        }

        $this->info("Creating menus...");
        foreach ($this->data as $menu) {
            $this->createMenu((array) $menu);
        }

        $this->line("\n");
        $this->info("Update menu successfully!");
        $this->info("------------------------");
        $this->info("Total: " . BaseModel::count() . " menus");
        
        return 0;
    }

    public function getLevels(array $menu): array {
        return isset($menu["levels"]) 
            ? $menu["levels"] 
            : ["L0", "L1", "L2"];
    }

    public function createMenu(array $menu, int $order = 0, string|int $mainId = null)
    {
        $label = $menu["label"];
        $code = $menu["code"];
        $url = $menu["url"] ?? null;
        // $url = implode("/", [strtolower($this->prefix), $url]);
        $levels = $this->getLevels($menu);

        // MAIN MENU
        if($mainId === null) {
            $code = strtoupper($this->prefix) . "_" . strtoupper($menu["code"]);
            
            $this->line("\n");
            $this->info($label);
            $this->line("========================");
            $this->line("CODE: {$code}");
            $this->line("LEVELS: " . implode(", ", $levels));
            $this->line("========================");
        } 
        
        // CHILD MENU
        if($mainId !== null) {
            $main = BaseModel::where("id", $mainId)->first();
            $code = $main->code . "_" . strtoupper($menu["code"]);

            $this->info("--> {$label}");
            $this->line("--> {$url}");
        }

        $model = BaseModel::updateOrCreate(["code" => $code], [
            "parent_id" => $mainId,
            "prefix" => $this->prefix,
            "type" => "link",
            "label" => $label,
            "icon" => @$menu["icon"] ?? null,
            "url" => $url ?? null,
            "sort" => 0,
            "active" => true,
            "levels" => $levels
        ]);

        if (isset($menu["items"])) {
            foreach ($menu["items"] as $key => $item) {
                $this->createMenu($item, $key, $model->id);
            }
        }
    }

}
