<?php

namespace Chaos\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Support\Facades\File;

class ChaosMigrate extends Command
{
    use ConfirmableTrait;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chaos:migrate {name?} {method?} {--migrate}';
    
    protected $length = 8;

    public function handle() {
        if (! $this->confirmToProceed()) {
            return 1;
        }
        
        $this->info("Running migrations...");
        foreach($this->getMigrationPaths() as $path) {
            $this->line("Migrate: {$path}");
            $this->runMigration($path);
        }
        
        $this->info("[OK] Migrate successfully!");
        return 0;
    }
    
    public function runMigration(string $path) {
        $method = $this->argument("method");
        if ($method) {
            $this->call("migrate:{$method}", ["--path" => $path]);
            if ($this->option("migrate")) {
                $this->call("migrate", ["--path" => $path]);
            }
        }
        else {
            $this->call("migrate", ["--path" => $path]);
        }
    }
                
    public function getMigrationPaths(): array {
        $results = [$this->getMigrationPathName("*")];
        $rows = File::directories(database_path("migrations"));
        foreach($rows as $row) {
            // Check if specific migration
            $name = substr(basename($row), $this->length);
            if($this->argument("name") && $this->argument("name") === $name) {
                $results = [$this->getMigrationPathName($row)];
                break;
            }
        }
        
        return array_merge($results);
    }

    public function getMigrationPathName(string $name): string {
        $name = basename($name);
        return "/database/migrations/{$name}";
    }

}
