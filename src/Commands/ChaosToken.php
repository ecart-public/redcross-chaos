<?php

namespace Chaos\Commands;

use Chaos\Auth\TokenService;
use Illuminate\Console\Command;

class ChaosToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chaos:token';
    
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $token = (new TokenService())->make();
        $this->info("Token: " . $token);
        return 0;
    }

}