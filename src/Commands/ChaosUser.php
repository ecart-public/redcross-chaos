<?php

namespace Chaos\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use Chaos\Models\CSUser;
use Chaos\Models\CSAuth;
use Chaos\Models\CSRole;
use Chaos\Models\CSRoleUser;

class ChaosUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'chaos:user {--reset}';
    
    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->runInLocal();
        return 0;
    }

    protected $super;

    protected $user;
    
    public function runTruncate()
    {
        if(!$this->option("reset")) {
            $this->line("[SKIP] Truncate all tables.");
            return;
        }
        
        CSRoleUser::truncate();
        CSRole::truncate();
        CSAuth::truncate();
        CSUser::truncate();
        
        $this->info("[OK] Truncate all tables.");
    }
    

    public function runInLocal()
    {
        $this->runTruncate();
        $this->CreateRole();
        $this->CreateSuper();
    }
    
    public function CreateRole(): void {
        $roles = config("chaos.role");
        foreach($roles as $role) {
            CSRole::create($role);
        }
        
        $this->user = CSRole::where("code", "USER")->first();
        $this->super = CSRole::where("code", "SUPER")->first();
    }

    public function CreateSuper(): void {
        $roles = CSRole::whereIn("code", ["USER", "REVIEWER", "SUPER"])->get();
        
        $this->CreateUser("super1@ecnmd.online", $roles);
        $this->CreateUser("super2@ecnmd.online", $roles);
        $this->CreateUser("super3@ecnmd.online", $roles);
    }
    
    public function CreateUser(string $username, $roles = [], $password = "admin1234"): void
    {
        DB::beginTransaction();
        
        $faker = \Faker\Factory::create();
        $user = CSUser::create([
            "name" => "นาย{$faker->firstName} {$faker->lastName}", 
            "picture" => [
                "path" => "assets/user.png",
                "url" => "assets/user.png",
            ],
            "email" => $faker->email,
            "data" => []
        ]);

        CSAuth::create([
            "uid" => $user->guid,
            "username" => $username,
            "password" => Hash::make($password)
        ]);

        foreach($roles as $role) {
            CSRoleUser::create([
                "uid" => $user->guid,
                "role" => $role->code,
                "name" => $role->name,
                "unit_id"=> 999
            ]);
        }

        DB::commit();
        $this->line("[OK] User: $username Password: $password");
    }
    
}