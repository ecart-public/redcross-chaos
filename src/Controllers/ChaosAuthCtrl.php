<?php

namespace Chaos\Controllers;

use Chaos\Auth\Authenticator;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;

class ChaosAuthCtrl extends Controller
{
   
    /**
     * @var Authenticator
     */
    protected $model;
    
    /**
     * ChaosAuthCtrl constructor.
     */
    public function __construct() {
        $data = request()->input("data", []);
        $this->model = config("chaos.auth.authenticator", Authenticator::class);
        $this->model = new $this->model($data);
    }
    
    /**
     * Sign In
     * 
     * @param Request $request
     * @return mixed
     */
    public function signIn(Request $request)
    {
        $method = Str::camel("sign_in" . Str::lower($request->type));
        $model = config("chaos.auth.authenticator", Authenticator::class);
        
        $refl = new \ReflectionClass($model);
        if($refl->hasMethod($method)) {
            $auth = new $model($request->input("data", []));
            return $auth->{$method}();
        }
        
        return response()->json([
            "result" => false,
            "message" => "Invalid Type: {$method}"
        ], 400);
    }
    
    /**
     * Sign Up
     * 
     * @param Request $request
     * @return mixed
     */
    public function signUp(Request $request)
    {
        $method = Str::camel("sign_up" . Str::lower($request->type));
        $model = config("chaos.auth.authenticator", Authenticator::class);
        
        $refl = new \ReflectionClass($model);
        if($refl->hasMethod($method)) {
            $auth = new $model($request->input("data", []));
            return $auth->{$method}();
        }
        
        return response()->json([
            "result" => false,
            "message" => "Invalid Type: {$method}"
        ], 400);
    }
    
    /**
     * Get Token
     * 
     * @return mixed
     */
    public function verifyOtp ()
    {
        return $this->model->verifyOtp();
    }
    
    /**
     * Get Token
     * 
     * @return mixed
     */
    public function refreshToken()
    {
        return $this->model->refreshToken();
    }
    
    /**
     * Get Token
     * 
     * @return mixed
     */
    public function switchTo()
    {
        return $this->model->switchTo();
    }
    
    /**
     * Sign Out
     * 
     * @return mixed
     */
    public function signOut()
    {
        return $this->model->signOut();
    }
    
}