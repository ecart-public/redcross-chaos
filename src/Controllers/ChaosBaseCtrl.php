<?php
namespace Chaos\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Str;
use Chaos\Support\Response;

class ChaosBaseCtrl extends Controller
{
    protected $type = "default";
    protected $data = [];

    public function index(Request $request) {
        $this->type = $request->input("type", "default");
        $this->data = $request->input("data", []);
        
        try {
            $method = Str::camel($this->type);
            if (!method_exists($this, $method)) {
                throw new Exception("Invalid type: " . $this->type);
            }
            return Response::Success($this->{$method}());

        } catch(Exception $ex) {
            return Response::Exception($ex);
        }
    }
    
}