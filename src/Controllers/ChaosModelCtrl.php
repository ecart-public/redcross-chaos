<?php
namespace Chaos\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Chaos\Support\Response;

class ChaosModelCtrl extends Controller {

    protected $model;
    protected $perPage = 20;

    public function getClass(): Model
    {
        return (new \ReflectionClass($this->model))->newInstance();
    }
    
    public function getStatic(): Model
    {
        return (new \ReflectionClass($this->model))->newInstanceWithoutConstructor();
    }

    public function index(Request $request)
    {
        return $this->getStatic()
            ->paginate($this->perPage);
    }

    public function show($id)
    {
        $result = $this->getStatic()::find($id);
        return $result 
            ? Response::Json($result->toArray())
            : Response::NotFound();
    }

    public function store(Request $request)
    {
        $objModel = $this->getClass();
        $objModel->fill($request->all());
        $objModel->save();

        return Response::Json($objModel->toArray());
    }

    public function update(Request $request, $id)
    {
        $objModel = $this->getStatic()::find($id);
        $objModel->fill($request->all());
        $objModel->save();

        return Response::Json($objModel->toArray());
    }

    public function destroy($id)
    {
        $this->getStatic()::find($id)->delete();
        return Response::Success();
    }
}
