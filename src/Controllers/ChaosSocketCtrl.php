<?php

namespace Chaos\Controllers;

use Chaos\Support\Auth;
use Pusher\Pusher;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ChaosSocketCtrl extends Controller
{
    
    public function index(Request $request)
    {
        $config = config('broadcasting.connections.pusher');
        $pusher = new Pusher($config['key'], $config['secret'], $config['app_id'], $config['options']);
        
        try {
            if (str_starts_with($request->channel_name, 'private')) {
                $response = method_exists($pusher, 'authorizeChannel')
                    ? $pusher->authorizeChannel($request->channel_name, $request->socket_id)
                    : $pusher->socket_auth($request->channel_name, $request->socket_id);
                        
                return response()->json(json_decode($response));
            }

            
            $broadcastIdentifier = json_encode([
                'user_id' => Auth::getUserId(),
                'user_info' => Auth::getUser()->name
            ]);
            $result = [];
            
            $response = method_exists($pusher, 'authorizePresenceChannel')
                ? $pusher->authorizePresenceChannel($request->channel_name, $request->socket_id, $broadcastIdentifier, $result)
                : $pusher->presence_auth($request->channel_name, $request->socket_id, $broadcastIdentifier, $result);
                
            return response()->json(json_decode($response));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 403);
        }
    }
    
    
}