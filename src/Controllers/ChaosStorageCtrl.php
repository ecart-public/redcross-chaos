<?php
namespace Chaos\Controllers;

use Chaos\Support\Chaos;
use Chaos\Support\Response;
use Chaos\Models\CSAsset;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use League\Flysystem\Filesystem;
use League\Flysystem\ZipArchive\ZipArchiveAdapter;
use League\Flysystem\ZipArchive\FilesystemZipArchiveProvider;

class ChaosStorageCtrl {

    protected $errors = [];
    
    public function index(Request $request)
    {
        return CSAsset::query()
            ->orderBy("created_at", "desc")
            ->paginate(30);
    }
    
    public function show(Request $request, $guid)
    {
        $result = CSAsset::where("guid", $guid)->first();
        if(!$result) {
            return Response::NotFound();
        }
        
        if($result->is_private && !Chaos::auth()->isLoggedIn()) {
            return Response::Unauthorized();   
        }
        
        $result->increment("downloads");
        return Storage::response($result->path, $result->name)
            ->setPublic()
            ->setMaxAge(now()->addDays(30)->diffInSeconds());
        
    }
    
    public function zip(Request $request)
    {
        $assets = CSAsset::whereIn("guid", $request->get("guids"))->get();
        if(count($assets) > 0) {
            $name = Str::random(16);
            $path = "temps/temp_{$name}.zip";
            
            $provider = new FilesystemZipArchiveProvider(storage_path("app/{$path}"));
            $adapter = new ZipArchiveAdapter($provider);
            $zip = new Filesystem($adapter);
            
            foreach($assets as $asset) {
                $content = Storage::get($asset->path);
                $zip->write($asset->name, $content);
            }
            
            return Storage::download($path);
        }
        
        return response("404 - Not found", 404);
    }

    public function checkUpload(Request $request) {
        $validator = Validator::make($request->all(), [
            "file" => "required|max:1000000" // 100MB
        ], [
            "file.required" => "จำเป็นต้องอัพโหลดไฟล์",
            "file.max" => "ขนาดของไฟล์จะต้องไม่เกิน :max KB",
        ]);

        if($validator->fails()) {
            $this->errors = $validator->errors();
            return false;
        } else {
            return true;
        }
    }

    public function getFileName(string $name, string $extension) {
        $path = implode("/", [date("Ym"), Str::uuid(), $name]);
        $path .= "." . $extension;

        return $path;
    }

    public function upload(Request $request)
    {
        if($this->checkUpload($request)) {
            try {
                $file = $request->file('file');

                $accessType = $request->input("access_type") ?? "private";
                $name = $request->input("file_name") ?? $file->getClientOriginalName();
                $size = floatval($file->getSize());
                $extension = $file->extension();
                
                $path = Storage::put($this->getFileName($name, $extension), $file);
                if (!$path) {
                    return response()->json([
                        "result" => false,
                        "message" => "ไม่สามารถบันทึกไฟล์",
                        "file" => [
                            "name" => $name,
                            "size"=> $size,
                            "extension" => $extension
                        ]
                    ], 400);
                }
                
                $result = CSAsset::create([
                    "name" => $name,
                    "extension" => $extension,
                    "path" => $path,
                    "size" => $size,
                    "access_type" => $accessType
                ]);

                return response()->json($result);

            } catch (\Exception $ex) {
                return response()->json([
                    "errors" => [],
                    "message" => $ex->getMessage()
                ], 400);
            }
        } else {
            return response()->json([
                "errors" => $this->errors,
                "message" => $this->errors->first()
            ], 400);
        }
    }

}
