<?php

namespace Chaos\Controllers;

use Chaos\Support\Response;
use Chaos\Models\CSStore;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ChaosStoreCtrl extends Controller
{
    
    public function index(Request $request)
    {
        $result = CSStore::query()
            ->take($request->take)
            ->offset($request->offset)
            ->get();
        
        return Response::Success($result);
    }
    
    public function store(Request $request)
    {
        $result = CSStore::create($request->all());
        return Response::Success($result);
    }
    
    public function show(Request $request, string $guid)
    {
        $result = CSStore::where("guid", $guid)->first();
        return $result ? Response::Success($result) : Response::NotFound();
    }
    
    public function update(Request $request, string $guid)
    {
        $result = CSStore::where("guid", $guid)->first();
        if(!$result) {
            return Response::NotFound();
        }
        
        $result->update($request->all());
        return Response::Success($result);
    }
    
    public function delete(Request $request, string $guid)
    {
        $result = CSStore::where("guid", $guid)->first();
        if(!$result) {
            return Response::NotFound();
        }
        
        $result->delete();
        return Response::Success();
    }
    
}