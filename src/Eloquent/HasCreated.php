<?php

namespace Chaos\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Chaos\Support\Auth;
use Chaos\Models\CSUser;

trait HasCreated {

    public static function bootHasCreated() {
        self::creating(function(Model $model) {
            if(!$model->created_by) {
                $model->created_by = Auth::getClaim("uid");
            }
        });
    }

    public function initializeHasCreated() {
        $this->append("created_name");
    }

    public function getCreatedNameAttribute() {
        $user = CSUser::findByGuid($this->created_by);
        return $user ? $user->name : "นำเข้าจากระบบเดิม";
    }
    
}
