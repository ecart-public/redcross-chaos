<?php

namespace Chaos\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait HasGuid {

    public static function bootHasGuid()
    {
        self::creating(function(Model $model) {
            if($model->guid == null) {
                $model->guid = Str::uuid()->toString();
            }
        });
    }
    
    public static function findByGuid(?string $guid)
    {
        if($guid == null) {
            return null;
        }
        return self::where("guid", $guid)->first();
    }
    
}
