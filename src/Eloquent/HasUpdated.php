<?php

namespace Chaos\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Chaos\Support\Auth;
use Chaos\Models\CSUser;

trait HasUpdated {

    public static function bootHasUpdated() {
        self::saving(function(Model $model) {
            if(!$model->updated_by) {
                $model->updated_by = Auth::getClaim("uid");
            }
        });
    }

    public function initializeHasUpdated() {
        $this->append("updated_name");
    }

    public function getUpdatedNameAttribute() {
        $user = CSUser::findByGuid($this->updated_by);
        return $user ? $user->name : "นำเข้าจากระบบเดิม";
    }
}
