<?php

namespace Chaos\Eloquent;

use Chaos\Models\CSUser;

trait HasUser {

    public function initializeHasUser() {
        $this->append("full_name");
        $this->append("picture");
    }
    
    public function user() {
        return $this->belongsTo(CSUser::class, "uid", "guid");
    }

    public function getFullNameAttribute() {
        $user = $this->user;
        return $user ? $user->name : null;
    }
    
    public function getPictureAttribute() {
        $user = $this->user;
        return $user ? $user->picture : [];
    }
    
}
