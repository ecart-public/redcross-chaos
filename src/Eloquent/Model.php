<?php

namespace Chaos\Eloquent;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel {
    
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->connection = config("chaos.app.connection", "chaos");
        $this->setConnection($this->connection);
    }
    
    protected function serializeDate(DateTimeInterface $date): string
    {
        return $date->format(DateTimeInterface::ISO8601);
    }
    
}
