<?php
 
namespace Chaos\Logging;
 
use Monolog\Logger;
use Monolog\Level;
use Monolog\LogRecord;
 
class ChaosLogger
{
    /**
     * Create a custom Monolog instance.
     */
    public function __invoke(array $config): Logger
    {
        return new Logger("chaos-logger", [
            new ChaosLoggerHandler
        ]);
    }
    
    static function getType(LogRecord $record): string
    {
        return $record->context["type"] ?? "general";
    }
    
    static function getLevel(LogRecord $record): array
    {
        $level = $record->level;
        return [
            "name" => $level->name,
            "value" => $level->value
        ];
    }
}