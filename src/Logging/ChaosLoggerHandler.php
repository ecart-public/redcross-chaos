<?php

namespace Chaos\Logging;

use Chaos\Models\CSLog;
use Illuminate\Support\Facades\Storage;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\LogRecord;

class ChaosLoggerHandler extends AbstractProcessingHandler
{
    
    protected $logProject = "default";
    protected $logStore = "database";
    
    
    public function write(LogRecord $record): void
    {
        if(!config("chaos.logging.enabled", false)) {
            return;
        }
        
        $this->logProject = config("chaos.app.project");
        $this->logStore = config("chaos.logging.store");
        
        switch($this->logStore) {
            case "database":
                $this->writeDatabase($record);
                break;
        }
        
        // Default
        $this->writeFile($record);
    }
    
    protected function writeDatabase(LogRecord $record): void
    {
        CSLog::create([
            "project" => $this->logProject,
            "type" => ChaosLogger::getType($record),
            "level" => ChaosLogger::getLevel($record),
            "message" => $record->message,
            "context" => [
                "formatted" => $record->formatted,
                "context" => $record->context
            ]
        ]);
    }
    
    protected function writeFile(LogRecord $record): void
    {
        $type = ChaosLogger::getType($record);
        $fileName = "logs/{$type}/" . "chaos_" . date("Ymd") . ".log";
        Storage::append($fileName, $record->formatted);
    }
    
}