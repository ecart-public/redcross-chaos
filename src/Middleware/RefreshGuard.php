<?php

namespace Chaos\Middleware;

use Closure;
use Illuminate\Http\Request;
use Chaos\Support\Response as ChaosResponse;
use Chaos\Auth\RefreshService;

class RefreshGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        if (!RefreshService::validate()) {
            return ChaosResponse::Unauthorized();
        }
        
        return $next($request);
    }
}
