<?php

namespace Chaos\Middleware;

use Closure;
use Illuminate\Http\Request;
use Chaos\Support\Response as ChaosResponse;
use Chaos\Auth\TokenService;

class TokenGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $roles = null) {
        $roles = $roles ? explode(",", trim($roles)) : [];
        
        if (!TokenService::validate($roles)) {
            return ChaosResponse::Unauthorized(["roles" => $roles]);
        }
        
        return $next($request);
    }
}
