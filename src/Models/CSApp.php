<?php

namespace Chaos\Models;

use Chaos\Eloquent\Model;

class CSApp extends Model
{
    protected $table = "cs_app";
    public $timestamps = true;
    
    protected $fillable = [
        "name",
        "short_name",
        "icon_url",
        "title",
        "description",
        "status",
        "maintence_expire",
        "certificate",
        "data",
        "options"
    ];
    
    protected $casts = [
        "status" => "boolean",
        "certificate" => "array",
        "data" => "array",
        "options" => "array"
    ];
    
}
