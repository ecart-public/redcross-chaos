<?php

namespace Chaos\Models;

use Chaos\Eloquent\Model;
use Chaos\Eloquent\HasGuid;

class CSAsset extends Model
{
    use HasGuid;

    protected $table = "cs_asset";
    public $timestamps = true;
    protected $fillable = [
        "name",
        "extension",
        "path",
        "size",
        "access_type",
        "data"
    ];

    protected $appends = [
        "is_private",
        "readable_size",
        "url"
    ];

    protected $casts = [
        "data" => "array"
    ];
    
    public function getReadableSizeAttribute() {
        $size = $this->size;
        $unit = "";
        
        if( (!$unit && $size >= 1<<30) || $unit == "GB")
          return number_format($size/(1<<30),2)."GB";
        if( (!$unit && $size >= 1<<20) || $unit == "MB")
          return number_format($size/(1<<20),2)."MB";
        if( (!$unit && $size >= 1<<10) || $unit == "KB")
          return number_format($size/(1<<10),2)."KB";
        
        return number_format($size)." bytes";
    }

    public function getUrlAttribute() {
        return route("cs_storage", ["guid" => strtoupper($this->guid)]);
    }
    
    public function getIsPrivateAttribute() {
        return strtolower($this->attributes["access_type"]) === "private";
    }
}
