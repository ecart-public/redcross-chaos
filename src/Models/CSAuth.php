<?php

namespace Chaos\Models;

use Chaos\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CSAuth extends Model
{
    protected $table = "cs_auth";
    protected $fillable = [
        "uid",
        "username",
        "password",
        "attempt",
        "attempt_at",
        "status",
        "expire_at",
        "data"
    ];
    
    protected $casts = [
        "data" => "array"
    ];
    
    public static function findByUsername(string $username)
    {
        return self::where("username", $username)->first();
    }
    
    public static function findByUid(string $uid)
    {
        return self::where("uid", $uid)->first();
    }
    
    public static function findActiveByUsername(string $username)
    {
        return self::where("username", $username)
            ->where("status", 1)
            ->first();
    }
    
    public static function attempt(string $username): bool
    {
        $auth = self::where("username", $username)->first();
        $time = now()->subMinutes(5)->getTimestamp();
        
        if ($auth->attempt >= 5 
            && $auth->attempt_at 
            && $auth->attempt_at > $time) {
                return false;
        }
        
        self::addAttempt($username);
        return true;
    }
    
    public static function addAttempt(string $username)
    {
        return self::where("username", $username)->update([
            "attempt" => DB::raw("attempt + 1"),
            "attempt_at" => time()
        ]);
    }
    
    public static function resetAttempt(string $username)
    {
        return self::where("username", $username)->update([
            "attempt" => 0,
            "attempt_at" => null
        ]);
    }

}
