<?php

namespace Chaos\Models;

use Chaos\Eloquent\Model;
use Chaos\Eloquent\HasGuid;

class CSClient extends Model
{
    use HasGuid;

    protected $table = "cs_client";
    public $timestamps = true;
    protected $fillable = [
        "client_id",
        "client_secret",
        "data",
        "ca"
    ];

    protected $casts = [
        "data" => "array",
        "ca" => "array"
    ];

}
