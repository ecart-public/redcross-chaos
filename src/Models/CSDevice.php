<?php

namespace Chaos\Models;

use Chaos\Eloquent\Model;

class CSDevice extends Model
{

    protected $table = "cs_device";
    public $timestamps = true;
    
    protected $fillable = [
        "uid",
        "device",
        "status",
        "last_activity",
        "data",
    ];

}
