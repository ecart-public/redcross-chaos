<?php

namespace Chaos\Models;

use Chaos\Eloquent\Model;

class CSLink extends Model
{
    protected $table = "cs_link";
    public $timestamps = true;
    protected $fillable = [
        "code",
        "name",
        "url",
        "visits",
        "expires_at",
        "status",
        "data"
    ];
}
