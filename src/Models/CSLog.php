<?php

namespace Chaos\Models;

use Chaos\Eloquent\Model;

class CSLog extends Model
{
    protected $table = "cs_log";
    public $timestamps = true;
    
    protected $fillable = [
        "project",
        "type",
        "message",
        "level",
        "context"
    ];
    
    protected $casts = [
        "level" => "array",
        "context" => "array"
    ];
    
}
