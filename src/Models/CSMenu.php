<?php

namespace Chaos\Models;

use Chaos\Eloquent\Model;

class CSMenu extends Model
{
    protected $table = "cs_menu";
    public $timestamps = true;
    
    protected $fillable = [
        "parent_id",
        "prefix",
        "code",
        "type",
        "label",
        "icon",
        "url",
        "sort",
        "active",
        "levels",
        "data",
    ];

    protected $casts = [
        "active" => "boolean",
        "sort" => "integer",
        "levels" => "array",
        "data" => "array",
    ];
    
    public function items()
    {
        return $this->hasMany(CSMenu::class, "parent_id");
    }

}
