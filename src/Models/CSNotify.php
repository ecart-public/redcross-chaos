<?php

namespace Chaos\Models;

use Chaos\Eloquent\HasGuid;
use Chaos\Eloquent\Model;

class CSNotify extends Model
{
    use HasGuid;
    protected $table = "cs_notify";
    public $timestamps = true;
    
    protected $fillable = [
        "type",
        "uid",
        "title",
        "message",
        "priority",
        "status",
        "sent_at",
        "read_at",
        "schedule",
        "attachment",
        "actions",
        "data",
    ];
    
    protected $casts = [
        "schedule" => "array",
        "attachment" => "array",
        "actions" => "array",
        "data" => "array",
    ];

}
