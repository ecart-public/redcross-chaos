<?php

namespace Chaos\Models;

use Chaos\Eloquent\HasGuid;
use Chaos\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CSPermission extends Model
{
    use HasGuid,
        HasFactory;

    protected $table = "cs_permission";
    public $timestamps = true;

}
