<?php

namespace Chaos\Models;

use Chaos\Eloquent\HasGuid;
use Chaos\Eloquent\Model;

class CSProvider extends Model
{
    use HasGuid;

    protected $table = "cs_provider";
    public $timestamps = true;

}
