<?php

namespace Chaos\Models;

use Chaos\Eloquent\HasGuid;
use Chaos\Eloquent\Model;

class CSRole extends Model
{
    use HasGuid;

    protected $table = "cs_role";
    public $timestamps = true;

    protected $fillable = [
        "code",
        "name",
    ];
    
}
