<?php

namespace Chaos\Models;

use Chaos\Eloquent\Model;

class CSRoleMenu extends Model
{
    protected $table = "cs_role_menu";
    public $timestamps = true;
    
    protected $fillable = [
        "role",
        "menu",
        "status"
    ];
    
    protected $casts = [
        "status" => "boolean"
    ];

}
