<?php

namespace Chaos\Models;

use Chaos\Eloquent\HasGuid;
use Chaos\Eloquent\Model;

class CSRolePermission extends Model
{
    use HasGuid;

    protected $table = "cs_role_permission";
    public $timestamps = true;

}
