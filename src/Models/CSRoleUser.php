<?php

namespace Chaos\Models;

use Chaos\Eloquent\HasGuid;
use Chaos\Eloquent\Model;

class CSRoleUser extends Model
{
    use HasGuid;

    protected $table = "cs_role_user";
    public $timestamps = true;

    protected $fillable = [
        "role",
        "uid",
        "name",
        "is_primary",
        "is_lock",
        "status",
        "expire_at",
        "data"
    ];
    
    protected $casts = [
        "is_primary" => "boolean",
        "is_lock" => "boolean",
        "status" => "boolean",
        "data" => "array",
    ];
    
    public static function findByUid(string $uid)
    {
        return self::where("uid", $uid)->withActive()->first();
    }
    
    public static function getByUid(string $uid)
    {
        return self::where("uid", $uid)->withActive()->get();
    }
    
    public function scopeWithActive($query)
    {
        return $query->where("status", 1)
            ->where(function($query) {
                $query->whereNull("expire_at")
                    ->orWhere("expire_at", ">", time());
            })
            ->orderBy("is_primary", "desc");
    }
       
    public function getRoleAttribute() 
    {
        return strtoupper($this->attributes["role"]);
    }
    
    public function getNameAttribute() 
    {
        return isset($this->attributes["name"])
            ? $this->attributes["name"]
            : $this->role;
    }
    
}
