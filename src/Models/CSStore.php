<?php

namespace Chaos\Models;

use Chaos\Eloquent\HasGuid;
use Chaos\Eloquent\Model;

class CSStore extends Model
{
    use HasGuid;

    protected $table = "cs_store";
    public $timestamps = true;

    protected $fillable = [
        "name",
        "data"
    ];

    protected $casts = [
        "data" => "array"
    ];
    
}
