<?php

namespace Chaos\Models;

use Chaos\Eloquent\HasGuid;
use Chaos\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class CSUser extends Model
{
    use HasGuid,
        Notifiable;

    protected $table = "cs_user";
    public $timestamps = true;
    
    protected $fillable = [
        "name",
        "picture",
        "email",
        "phone",
        "data",
    ];
    
    protected $casts = [
        "picture" => "array",
        "phone" => "array",
        "data" => "array",
    ];

}
