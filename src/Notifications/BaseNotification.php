<?php

namespace Chaos\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\BroadcastMessage;

class BaseNotification extends Notification
{
    use Queueable;
    
    protected $type;
    protected $title;
    protected $message;

    /**
     * Create a new notification instance.
     */
    public function __construct(string $title, string $message, string $type = "SYSTEM")
    {
        $this->type = $type;
        $this->title = $title;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [ChaosChannel::class, 'broadcast'];
    }
    
    public function toChaos(object $notifiable): array
    {
        return [
            "type" => $this->type,
            "title" => $this->title,
            "message" => $this->message
        ];
    }
    
    public function toBroadcast(object $notifiable): BroadcastMessage
    {
        return new BroadcastMessage([
            "type" => $this->type,
            "title" => $this->title,
            "message" => $this->message
        ]);
    }

}
