<?php
 
namespace Chaos\Notifications;
 
use Chaos\Models\CSNotify;
 
class ChaosChannel
{
    /**
     * Send the given notification.
     */
    public function send(object $notifiable, object $notification): void
    {
        CSNotify::create([
            "uid" => $notifiable->guid,
            ...$notification->toChaos($notifiable),
            "read_at" => null
        ]);
    }
}

