<?php

namespace Chaos\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OtpNotification extends Notification
{
    use Queueable;
    
    protected $referCode;
    
    protected $otp;

    /**
     * Create a new notification instance.
     */
    public function __construct(string $referCode, string $otp)
    {
        $this->referCode = $referCode;
        $this->otp = $otp;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('บริการแจ้งเตือน รหัสยืนยัน OTP')
            ->line('รหัสยืนยันสำหรับเข้าสู่ระบบของคุณคือ ')
            ->line($this->otp)
            ->line('Ref: ' . $this->referCode);
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
