<?php

namespace Chaos\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasswordChangeNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return [ChaosChannel::class, 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        $now = Carbon::now()->timezone('Asia/Bangkok');
        return (new MailMessage)
            ->subject('บริการแจ้งเตือนการเปลี่ยนแปลงรหัสผ่าน')
            ->line('เรียนผู้ใช้งาน')
            ->line('ระบบได้ตรวจพบการเปลี่ยนแปลงรหัสผ่านของท่าน โดยมีรายละเอียดดังนี้')
            ->line('เวลา: ' . $now->format('d/m/Y H:i:s'))
            ->line('อีเมลฉบับนี้ เป็นการแจ้งข้อมูลผ่านระบบอัติโนมัติ หากท่านไม่ได้ทำรายการตามรายละเอียดข้างต้น')
            ->line('กรุณาติดต่อผู้ดูแลระบบของท่าน เพื่อเปลี่ยนรหัสผ่านหรือป้องกันการเข้าถึงระบบจากบุคคลที่ไม่พึงประสงค์')
            ->line('ขอบคุณที่ใช้บริการของเรา');
    }

    public function toChaos(object $notifiable): array
    {
        $now = Carbon::now()->timezone('Asia/Bangkok');
        $messages = [
            "ท่านได้ทำการเปลี่ยนแปลงรหัสผ่านของท่าน",
            'เมื่อวันเวลา: ' . $now->format('d/m/Y H:i:s')
        ];
        
        return [
            "type" => "System",
            "title" => "บริการแจ้งเตือนการเปลี่ยนแปลงรหัสผ่าน",
            "message" => implode("<br>", $messages)
        ];
    }
}
