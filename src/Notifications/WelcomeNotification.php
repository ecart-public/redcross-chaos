<?php

namespace Chaos\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WelcomeNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('บริการแจ้งเตือน การลงทะเบียนใช้งาน NMD SUBMISSION ONLINE')
            ->line('เรียนผู้ใช้งาน')
            ->line('ระบบได้รับการลงทะเบียนข้อมูลของท่านเรียบร้อยแล้ว')
            ->line('อีเมลฉบับนี้ เป็นการแจ้งข้อมูลผ่านระบบอัติโนมัติ หากท่านไม่ได้ทำรายการตามรายละเอียดข้างต้น')
            ->line('กรุณาติดต่อผู้ดูแลระบบของท่าน เพื่อเปลี่ยนรหัสผ่านหรือป้องกันการเข้าถึงระบบจากบุคคลที่ไม่พึงประสงค์')
            ->line('ขอบคุณที่ใช้บริการของเรา');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
