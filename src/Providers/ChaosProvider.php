<?php

namespace Chaos\Providers;
 
use Illuminate\Support\ServiceProvider;
use Chaos\Logging\ChaosLogger;
use Chaos\Commands\ChaosToken;
use Chaos\Commands\ChaosUser;
use Chaos\Commands\ChaosMenu;
 
class ChaosProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if(method_exists(object_or_class: $this, method: 'addProviderToBootstrapFile')) {
            ServiceProvider::addProviderToBootstrapFile(ChaosProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Config
        $this->publishes([
            __DIR__.'/../config/app.php' => config_path('chaos/app.php'),
            __DIR__.'/../config/menu.php' => config_path('chaos/menu.php'),
        ], 'chaos-config');
        
        // Migrations
        $this->loadMigrationsFrom(database_path('/migrations/*'));
 
        // Commands
        $this->commands([
            ChaosMenu::class,
            ChaosToken::class,
            ChaosUser::class
        ]);

        // Routes
        $this->loadRoutesFrom(__DIR__.'/../routes/chaos.php');

        // Middleware
        $this->app['router']->aliasMiddleware('chaos.token', \Chaos\Middleware\TokenGuard::class);
        $this->app['router']->aliasMiddleware('chaos.refresh', \Chaos\Middleware\RefreshGuard::class);
        
        // Logging
        $this->app->make('config')->set('logging.channels.chaos', [
            'driver' => 'custom',
            'via' => ChaosLogger::class
        ]);
    }
}