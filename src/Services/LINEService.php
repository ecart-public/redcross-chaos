<?php
namespace Chaos\Services;
use Illuminate\Support\Facades\Request;

class LINEService {

    /**
     * LINE API endpoint
     */
    protected $LINE_API = "https://api.line.me";
    protected $LINE_CHANNEL_ID = "";
    protected $LINE_CHANNEL_SECRET = "";
    protected $LINE_REDIRECT_URI = "http://localhost:8000/auth/line/callback";

    protected $status = 200;
    protected $response = [];

    public function __construct() {
        $this->LINE_CHANNEL_ID = config("chaos.auth.line.client_id");
        $this->LINE_CHANNEL_SECRET = config("chaos.auth.line.client_secret");
        $this->LINE_REDIRECT_URI = config("chaos.auth.line.redirect_uri");
    }
    
    public function getClient()
    {
        return new \GuzzleHttp\Client([
            'base_uri' => $this->LINE_API
        ]);
    }
    
    public function getErrors(): array
    {
        return $this->status !== 200 
            ? $this->response 
            : [];
    }
    
    public function getResponse(): array
    {
        return $this->status === 200
            ? $this->response
            : [];
    }
    
    public function getStatusCode(): int
    {
        return $this->status;
    }
    
    public function isSuccess(): bool
    {
        return $this->status === 200;
    }
    
    public function setResponse($response): self
    {
        $this->status = $response->getStatusCode();
        $this->response = json_decode($response->getBody()->getContents(), true);
        
        return $this;
    }
    
    public function getProfileByIdToken($token): self
    {
        $response = $this->getClient()
            ->request('POST', 'oauth2/v2.1/verify', [
                "form_params" => [
                    "id_token" => $token,
                    "client_id" => $this->LINE_CHANNEL_ID
                ],
                "http_errors" => false
            ]);
        
        return $this->setResponse($response);
    }
    
    public function getByAuthorizationCode($code): self
    {
        $response = $this->getClient()
            ->request('POST', 'oauth2/v2.1/token', [
                "form_params" => [
                    "code" => $code,
                    "grant_type" => "authorization_code",
                    "client_id" => $this->LINE_CHANNEL_ID,
                    "client_secret" => $this->LINE_CHANNEL_SECRET,
                    "redirect_uri" => $this->LINE_REDIRECT_URI
                ],
                "http_errors" => false
            ]);

        return $this->setResponse($response);
    }

}