<?php

namespace Chaos\Support;
use Chaos\Auth\TokenService;
use Chaos\Models\CSUser;
use Chaos\Models\CSRoleUser;
use Illuminate\Support\Facades\Request;

class Auth
{
    public static function isLoggedIn() {
        return TokenService::validate();
    }
    
    public static function getClaim(string $name, $default = null) {
        return TokenService::getClaim($name, $default);
    }
    
    public static function getClaims() {
        return TokenService::getClaims();
    }
    
    public static function getBearerToken() {
        return Request::bearerToken();
    }
    
    public static function getUserId()
    {
        return self::getClaim("uid");
    }
    
    public static function getEmail()
    {
        return self::getClaim("email");
    }
    
    public static function getUser()
    {
        return CSUser::findByGuid(self::getUserId());
    }
    
    public static function getRole()
    {
        return CSRoleUser::findByUid(self::getUserId());
    }
    
    public static function getRoleCode()
    {
        return self::getRole()->role;
    }
    
    public static function hasRoles(array $roles)
    {
        $role = self::getRoleCode();
        return in_array($role, $roles);
    }
    
}