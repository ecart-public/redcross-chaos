<?php
namespace Chaos\Support;

use Chaos\Support\Auth;
use Chaos\Support\Response;

class Chaos
{
    public static function auth(): Auth {
        return (new \ReflectionClass(Auth::class))
            ->newInstanceWithoutConstructor();
    }
    
    public static function response(): Response {
        return (new \ReflectionClass(Response::class))
            ->newInstanceWithoutConstructor();
    }
    
    
}