<?php

namespace Chaos\Support;

use Exception;
use Throwable;
use Illuminate\Support\Facades\Cache;
use Response as BaseResponse;

class Response {
    
    public static function CacheRemember($key, $response, $ttl = 86400) {
        return Cache::remember($key, $ttl, function() use ($response) {
            return $response;
        }); 
    }
    
    public static function CacheJson($response) {
        return BaseResponse::json($response, 200)
            ->setPublic()
            ->setMaxAge(86400);
    }

    public static function Json($response) {
        return BaseResponse::json($response, 200);
    }
    
    public static function Success($response = [], int $maxAge = 0) {
        return response()
            ->json([
                "status" => "success",
                "response" => $response
            ])
            ->setStatusCode(200)
            ->setPublic()
            ->setMaxAge($maxAge);
    }
    
    public static function Unauthorized($response = [], string $message = "Unauthorized") {
        return BaseResponse::json([
            "status" => "error",
            "error" => true,
            "message" => $message,
            "response" => $response
        ], 401);
    }
    
    public static function BadRequest($response = [], string $message = "Bad Request") {
        return BaseResponse::json([
            "status" => "error",
            "error" => true,
            "message" => $message,
            "response" => $response
        ], 400);
    }
    
    public static function NotFound($response = [], string $message = "Not Found") {
        return BaseResponse::json([
            "status" => "error",
            "error" => true,
            "message" => $message,
            "response" => $response
        ], 404);
    }
    
    public static function Error($response = [], string $message = "System error", string $code = "E000") {
        return BaseResponse::json([
            "status" => "error",
            "error" => true,
            "code" => $code,
            "message" => $message,
            "response" => $response
        ], 404);
    }
    
    public static function Exception(Exception|Throwable $ex) {
        return response()
            ->json([
                "status" => "error",
                "error" => true,
                "response" => [],
                "code" => $ex->getCode(),
                "message" => $ex->getMessage(),
                "file" => $ex->getFile(),
                "line" => $ex->getLine(),
                "trace" => $ex->getTrace()
            ], 400)
            ->withHeaders([
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Methods' => 'GET, POST, PUT, DELETE, OPTIONS',
                'Access-Control-Allow-Headers' => '*',
                'Access-Control-Allow-Credentials' => 'true'
            ]);
    }

    public static function Redirect(string $url) {
        return BaseResponse::redirectTo($url);
    }
}
