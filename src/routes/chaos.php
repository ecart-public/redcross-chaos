<?php

use Chaos\Controllers\ChaosCtrl;
use Chaos\Controllers\ChaosAuthCtrl;
use Chaos\Controllers\ChaosSocketCtrl;
use Chaos\Controllers\ChaosStorageCtrl;
use Chaos\Middleware\TokenGuard;

/**
 * -----------
 * Chaos
 * -----------
 */
Route::group(["prefix" => "api/@chaos"], function () {
    Route::post("/", [ChaosCtrl::class, "index"]);
});

/**
 * -----------
 * Auth
 * -----------
 */
Route::group(["prefix" => "api/@chaos/auth"], function () {
    Route::post("", [ChaosAuthCtrl::class, "signIn"]);
    Route::post("signUp", [ChaosAuthCtrl::class, "signUp"]);
    Route::post("verifyOtp ", [ChaosAuthCtrl::class, "verifyOtp"]);
    Route::post("switchTo", [ChaosAuthCtrl::class, "switchTo"]);
    Route::post("refreshToken", [ChaosAuthCtrl::class, "refreshToken"]);
    Route::post("signOut", [ChaosAuthCtrl::class, "signOut"]);
});

/**
 * -----------
 * Storage
 * -----------
 */
Route::group(["prefix" => "api/@chaos/storage"], function () {
    Route::get("/", [ChaosStorageCtrl::class, "index"]);
    Route::get("/{guid}", [ChaosStorageCtrl::class, "show"])->name("cs_storage");
    
    Route::post("/", [ChaosStorageCtrl::class, "upload"])->middleware(TokenGuard::class);
    Route::post("/zip", [ChaosStorageCtrl::class, "downloadZip"]);
});


/**
 * -----------
 * Socket
 * -----------
 */
Route::post("api/@chaos/socket", [ChaosSocketCtrl::class, "index"])->middleware(TokenGuard::class);